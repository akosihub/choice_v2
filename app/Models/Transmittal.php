<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use League\Csv\Reader;

class Transmittal extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'transmittals';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
    public $csv = '';
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function boot() {
        parent::boot();

        self::saved(function ($model) {
            foreach($model->csv as $data)
            {
                $t_data = new Transmittal_Data(['data' => json_encode($data,JSON_PRETTY_PRINT)]);
                $model->transmittal_data()->save($t_data);
            }
        });
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function transmittal_data() {
        return $this->hasMany('App\Models\Transmittal_Data');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function setTransmittalcsvAttribute($value)
    {
        $csv_data = Reader::createFromPath($value->path());
        $this->csv = $csv_data; //json_encode($csv_data);
        
    }
}
