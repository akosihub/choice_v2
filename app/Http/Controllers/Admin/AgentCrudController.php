<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AgentRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

use App\Models\Bank;
use App\Models\Agent;
/**
 * Class AgentCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class AgentCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
//    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as traitStore; };

    public function setup()
    {
        $this->crud->setModel('App\Models\Agent');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/agent');
        $this->crud->setEntityNameStrings('agent', 'agents');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        //$this->crud->setFromDb();
        $this->crud->addColumn([
            'name' => 'first_name',
            'type' => 'text',
            'label' => 'First Name'
        ]);

        $this->crud->addColumn([
            'name' => 'last_name',
            'type' => 'text',
            'label' => 'Last Name'
        ]);

        $this->crud->addColumn([
            'name' => 'email',
            'type' => 'text',
            'label' => 'Email'
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(AgentRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        //$this->crud->setFromDb();
        $this->crud->addFields([[
            'name' => 'first_name',
            'type' => 'text',
            'label' => 'First Name',
            'tab' => 'Personal Information'
        ],
        [
            'name' => 'last_name',
            'type' => 'text',
            'label' => 'Last Name',
            'tab' => 'Personal Information'
        ],
        [
            'name' => 'email',
            'type' => 'email',
            'label' => 'Email',
            'tab' => 'Personal Information'
        ],
        [
            'name' => 'contract_date',
            'type' => 'date',
            'label' => 'Contract Date',
            'tab' => 'Employment Information'
        ],
        [
            'name' => 'start_date',
            'type' => 'date',
            'label' => 'Start Date',
            'tab' => 'Employment Information'
        ],
        [
            'name' => 'end_date',
            'type' => 'date',
            'label' => 'End Date',
            'tab' => 'Employment Information'
        ]
        ]);
        /*$this->crud->addField(
        [
            'name' => 'banks',
            'label' => 'Bank Codes',
            'type' => 'repeatable',
            'fields' => [
                [
                    'name' => 'bank_id',
                    'label' => 'Bank',
                    'type' => 'select2',
                    'entity' => 'banks',
                    'model' => 'Bank',
                    'pivot' => true
                ],
                [
                    'name' => 'code',
                    'label' => 'code',
                    'type' => 'text'
                ]
            ]
        ]);*/
        $this->crud->addFields(
        [
            [
                'name' => 'banks',
                'label' => 'Bank',
                'type' => 'select2',
                'entity' => 'banks',
                'attribute' => 'name',
                'fake' => true,
                'tab' => 'Bank Codes'
            ],
            [
                'name' => 'code',
                'label' => 'Code',
                'type' => 'text',
                'fake' => true,
                'tab' => 'Bank Codes'
            ]
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
