<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\BankRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class BankCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class BankCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Bank');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/bank');
        $this->crud->setEntityNameStrings('bank', 'banks');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        //$this->crud->setFromDb();
        $this->crud->addColumn([
            'name' => 'name',
            'type' => 'text',
            'label' => 'Bank'
        ]);

        $this->crud->addColumn([
            'name' => 'system_id',
            'type' => 'text',
            'label' => 'System ID'
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(BankRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->addField([
            'name' => 'name',
            'type' => 'text',
            'label' => 'Bank'
        ]);

        $this->crud->addField([
            'name' => 'system_id',
            'type' => 'text',
            'label' => 'System ID'
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
