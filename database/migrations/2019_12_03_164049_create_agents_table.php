<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAgentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('agents', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('photo', 65535)->nullable();
			$table->string('last_name', 50)->index('agent_last_name_index');
			$table->string('first_name', 50)->index('agent_first_name_index');
			$table->string('middle_name', 50)->nullable();
			$table->string('emergency_contact_name')->nullable();
			$table->integer('emergency_contact_number')->nullable();
			$table->string('email', 100);
			$table->string('password');
			$table->timestamps();
			$table->string('id_number', 100)->nullable();
			$table->date('contract_date')->nullable();
			$table->date('start_date')->nullable();
			$table->date('end_date')->nullable();
			$table->string('designation', 100)->nullable();
			$table->integer('mobile_globe')->nullable();
			$table->integer('mobile_smart')->nullable();
			$table->integer('mobile_other')->nullable();
            $table->text('banks')->nullable();
			$table->text('notes', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('agents');
	}

}
