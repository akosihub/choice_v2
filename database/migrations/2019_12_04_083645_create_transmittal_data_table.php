<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransmittalDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transmittal_data', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transmittal_id');
            $table->integer('agent_id');
            $table->integer('bank_id');
            $table->text('data');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transmittal_data');
    }
}
